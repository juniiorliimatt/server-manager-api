package br.com.server.repository;

import br.com.server.model.Server;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 11/11/2021
 */
@Repository
public interface ServerRepository extends JpaRepository<Server, Long>{
  Server findByIpAddress(String ipAddres);
  Server findByName(String name);
}
