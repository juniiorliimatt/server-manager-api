package br.com.server.model;

import br.com.server.enumeration.Status;
import br.com.server.resource.dto.ServerDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

import static javax.persistence.GenerationType.AUTO;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 11/11/2021
 */

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Server implements Serializable{

  @Id
  @GeneratedValue(strategy = AUTO)
  private Long id;

  @Column(unique = true)
  @NotEmpty(message = "IP Address cannot be empty or null")
  private String ipAddress;
  private String name;
  private String memory;
  private String type;
  private String imageUrl;
  private Status status;

  public Server(ServerDto dto){
    this.id = dto.getId();
    this.ipAddress = dto.getIpAddress();
    this.name = dto.getName();
    this.memory = dto.getMemory();
    this.type = dto.getType();
    this.imageUrl = dto.getImageUrl();
    this.status = dto.getStatus();
  }
}
