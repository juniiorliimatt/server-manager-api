package br.com.server.resource;

import br.com.server.enumeration.Status;
import br.com.server.model.Response;
import br.com.server.model.Server;
import br.com.server.resource.dto.ServerDto;
import br.com.server.service.ServerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static java.time.LocalDateTime.now;
import static java.util.Map.of;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 11/11/2021
 */

@RestController
@RequestMapping("/server")
@RequiredArgsConstructor
public class ServerResource{
  private final ServerService service;
  private static final String SERVER = "server";

  @GetMapping("/list")
  public ResponseEntity<Response> getServers() throws InterruptedException{
    TimeUnit.SECONDS.sleep(3);
    return ResponseEntity.ok(
      Response.builder()
        .timeStamp(now())
        .data(of("servers", service.list(30)))
        .message("Servers retrieved")
        .status(OK)
        .statusCode(OK.value())
        .build()
    );
  }

  @GetMapping("/ping/{ipAddress}")
  public ResponseEntity<Response> pingServer(@PathVariable("ipAddress") String ipAddress) throws IOException{
    Server server = service.ping(ipAddress);
    return ResponseEntity.ok(
      Response.builder()
        .timeStamp(now())
        .data(of(SERVER, server))
        .message(server.getStatus() == Status.SERVER_UP ? "Ping success" : "Ping failed")
        .status(OK)
        .statusCode(OK.value())
        .build()
    );
  }

  @PostMapping("/save")
  public ResponseEntity<Response> save(@Valid @RequestBody ServerDto dto){
  Server server = new Server(dto);
    return ResponseEntity.ok(
      Response.builder()
        .timeStamp(now())
        .data(of(SERVER, service.create(server)))
        .message("Server created")
        .status(CREATED)
        .statusCode(CREATED.value())
        .build()
    );
  }

  @GetMapping("/get/{id}")
  public ResponseEntity<Response> getServer(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
      Response.builder()
        .timeStamp(now())
        .data(of(SERVER, service.get(id)))
        .message("Server retrieved")
        .status(OK)
        .statusCode(OK.value())
        .build()
    );
  }

  @DeleteMapping("/delete/{id}")
  public ResponseEntity<Response> deleteServer(@PathVariable("id") Long id) {
    return ResponseEntity.ok(
      Response.builder()
        .timeStamp(now())
        .data(of("deleted", service.delete(id)))
        .message("Server deleted")
        .status(OK)
        .statusCode(OK.value())
        .build()
    );
  }

  @GetMapping(path = "/image/{fileName}", produces = IMAGE_PNG_VALUE)
  public byte[] getServerImage(@PathVariable("fileName") String fileName) throws IOException{
  return Files.readAllBytes(Paths.get(Path.of("C:/dev/web/server-manage/server-manage-api/src/main/resources/static") + "/" + fileName));
  }
}
