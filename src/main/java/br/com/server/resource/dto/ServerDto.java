package br.com.server.resource.dto;

import br.com.server.enumeration.Status;
import br.com.server.model.Server;

public class ServerDto{

  private final Long id;
  private final String ipAddress;
  private final String name;
  private final String memory;
  private final String type;
  private final String imageUrl;
  private final Status status;

  public ServerDto(Server server){
    this.id = server.getId();
    this.ipAddress = server.getIpAddress();
    this.name = server.getName();
    this.memory = server.getMemory();
    this.type = server.getType();
    this.imageUrl = server.getImageUrl();
    this.status = server.getStatus();
  }

  public ServerDto(Long id, String ipAddress, String name, String memory, String type, String imageUrl, Status status){
    this.id = id;
    this.ipAddress = ipAddress;
    this.name = name;
    this.memory = memory;
    this.type = type;
    this.imageUrl = imageUrl;
    this.status = status;
  }

  public Long getId(){
    return id;
  }

  public String getIpAddress(){
    return ipAddress;
  }

  public String getName(){
    return name;
  }

  public String getMemory(){
    return memory;
  }

  public String getType(){
    return type;
  }

  public String getImageUrl(){
    return imageUrl;
  }

  public Status getStatus(){
    return status;
  }
}
