package br.com.server.enumeration;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 11/11/2021
 */
public enum Status{
  SERVER_UP("SERVER_UP"),
  SERVER_DOWN("SERVER_DOWN");

  private final String status;

  Status(String status){
    this.status = status;
  }

  public String getStatus(){
    return status;
  }
}
