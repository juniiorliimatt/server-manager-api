package br.com.server.service;

import br.com.server.model.Server;

import java.io.IOException;
import java.util.Collection;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 11/11/2021
 */

public interface ServerService{
  Server create(Server server);
  Server ping(String ipAddress) throws IOException;
  Collection<Server> list(int limit);
  Server get(Long id);
  Server update(Server server);
  Boolean delete(Long id);
}
